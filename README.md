## Game Rest API's
To be able to run the API's you need to have an ArangoDB database running on port 8529 (can be changed within the class 
ArangoConfiguration present in both API projects). If you do not have an ArangoDB instance installed you can find 
instruction on how to install one here: 

https://docs.arangodb.com/3.3/Manual/GettingStarted/Installing/

Another solution is to run the docker image provided here:

https://hub.docker.com/r/arangodb/arangodb/

### Application
To build the project run:
``` 
mvn clean install
```
To run the project run:
```
java jar target/quizemon-game-rest-api-0.0.1.beta.jar
```
You can also run the project from within your development environment by running the class "com.quizemon.GameRestApplication"

The application connects per default to ArangoDB by the following credentials:
```
Host: localhost
Port: 8529
User: root
Password: test
```
To override the default it is possible to define the following environment variables: 
 - ARANGO_HOST 
 - ARANGO_PORT 
 - ARANGO_ROOT_USER 
 - ARANGO_PASSWORD

#### Docs
When application is started you can find swagger documentation under:
```
http://localhost:8081/swagger-ui.html
```
