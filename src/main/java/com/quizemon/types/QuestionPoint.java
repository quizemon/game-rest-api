package com.quizemon.types;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@ApiModel(
  value = "QuestionPoint",
  description = "A geographic location given by latitude and longitude in WGS84 format"
)
public class QuestionPoint {

  @ApiModelProperty(
    value = "A unique identyfier"
  )
  private String id;

  @ApiModelProperty(
    value = "Gives the north/south position.",
    allowableValues = "range[-90, 90]"
  )
  private Double latitude;

  @ApiModelProperty(
    value = "Gives the west/east position.",
    allowableValues = "range[-180, 180]"
  )
  private Double longitude;

}
