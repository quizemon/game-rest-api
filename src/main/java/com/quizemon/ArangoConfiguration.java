package com.quizemon;

import com.arangodb.ArangoDB;
import com.arangodb.ArangoDB.Builder;
import com.arangodb.springframework.annotation.EnableArangoRepositories;
import com.arangodb.springframework.config.AbstractArangoConfiguration;
import com.quizemon.utils.SysEnvUtil;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableArangoRepositories(basePackages = {"com.quizemon.arangorepositories"})
public class ArangoConfiguration extends AbstractArangoConfiguration {

  @Override
  public Builder arango() {

    // @formatter:off
    String host =     SysEnvUtil.getenv("ARANGO_HOST","localhost");
    Integer port =    SysEnvUtil.getenv("ARANGO_PORT", 8529);
    String user =     SysEnvUtil.getenv("ARANGO_USER","test");
    String password = SysEnvUtil.getenv("ARANGO_PASSWORD", "test");
    // @formatter:on

    return new ArangoDB.Builder().host(host, port).user(user).password(password);
  }

  @Override
  public String database() {
    return SysEnvUtil.getenv("ARANGO_DATABASE", "quizemon");
  }
}
