package com.quizemon.controllers;

import com.quizemon.services.QuestionPointService;
import com.quizemon.types.QuestionPoint;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/questionpoints")
@Api(
  value = "/questionpoints",
  description = "Methods for fetching question-points to present on a map"
)
public class QuestionPointController {

  private final QuestionPointService questionPointService;

  public QuestionPointController(QuestionPointService questionPointService) {
    this.questionPointService = questionPointService;
  }

  @GetMapping
  @ApiOperation(
    value = "Fetch question-points within an area",
    response = QuestionPoint.class,
    responseContainer = "List"
  )
  @ApiResponses(
    @ApiResponse(
      code = 400,
      message = "Bad request"
    )
  )
  public List<QuestionPoint> getQuestionPointsWithinArea(
    @RequestParam(required = false)
    @ApiParam(
      value = "Latitude in WGS84 format. Latitude goes from -90 to 90 and gives the north/south position.",
      required = true
    )
    Double lat,

    @RequestParam(required = false)
    @ApiParam(
      value = "Longitude in WGS84 format. Longitude goes from -180 to 180 and gives the west/east position.",
      required = true
    )
    Double lng,

    @RequestParam(value = "distance", required = false, defaultValue = "1000")
    @ApiParam(
      value = "Distance (in meters) within search for question-points is done. Defaults to 1000 if not provided.",
      defaultValue = "1000"
    )
    int distance ) {

    return questionPointService.getWithinDistance(lat, lng, distance);
  }

}
