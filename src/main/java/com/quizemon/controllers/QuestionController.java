package com.quizemon.controllers;

import com.quizemon.services.QuestionService;
import com.quizemon.types.Question;
import com.quizemon.types.QuestionResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
//TODO: Add swagger annotations for API documentations!!!!!!
@RestController
public class QuestionController {

  private final QuestionService questionService;

  public QuestionController(QuestionService questionService) {
    this.questionService = questionService;
  }

  @GetMapping("/randomquestion")
  public Question get() {
    return questionService.getRandom();
  }
  //TODO: Add test
  @PostMapping("questions/{id}/response")
  public QuestionResponse post(@PathVariable String id, @Valid @RequestBody QuestionResponse response) {
    return questionService.validateResponse(id, response);
  }
}
