package com.quizemon.services;

import com.quizemon.ResponseExceptions;
import com.quizemon.domain.dao.QuestionDao;
import com.quizemon.domain.repo.QuestionRepo;
import com.quizemon.types.Question;
import com.quizemon.types.QuestionResponse;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class QuestionService {

  private final QuestionRepo questionRepo;

  public QuestionService(QuestionRepo questionRepo) {
    this.questionRepo = questionRepo;
  }

  /**
   * Fetch a random question
   * @return a Question
   * @see Question
   */
  public Question getRandom() {
    return questionRepo.getRandom().toResponseType();
  }

  /**
   * Validate a given response
   * @param   id        The id of the question you want to give an answer to.
   * @param   response  QuestionResponse where you provide an answer
   * @return            A QuestionResponse saying if your answer was correct or not, and what the correct answer is.
   * @see     QuestionResponse
   */
  public QuestionResponse validateResponse(String id, QuestionResponse response) {
    QuestionDao questionDao = questionRepo.findOne(id);
    if(questionDao == null) {
      throw new ResponseExceptions.NotFoundException();
    }

    String correctAnswer = questionDao.getCorrectAnswer();

    response.setId(questionDao.getId());
    response.setCorrectAnswer(correctAnswer);
    response.setCorrect(Objects.equals(response.getAnswer(), correctAnswer));

    return response;
  }
}
