package com.quizemon.services;

import com.quizemon.domain.dao.QuestionPointDao;
import com.quizemon.domain.repo.QuestionPointRepo;
import com.quizemon.types.QuestionPoint;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@Component
public class QuestionPointService {

  private final QuestionPointRepo questionPointRepo;

  public QuestionPointService(QuestionPointRepo questionPointRepo) {
    this.questionPointRepo = questionPointRepo;
  }

  /**
   * Fetch question-points within an area
   * @param   lat           Latitude in WGS84 format. Latitude goes from -90 to 90 and gives the north/south position.
   * @param   lng           Longitude in WGS84 format. Longitude goes from -180 to 180 and gives the west/east position.
   * @param   distance      Distance (in meters) within search for question-points is done. Defaults to 1000 if not provided.
   * @return                All questionpoint within distance from the location provided with lat and lng
   * @See     QuestionPoint
   */
  public List<QuestionPoint> getWithinDistance(Double lat, Double lng, int distance) {
    Stream<QuestionPointDao> questionPointStream = null;
    if(lat != null && lng != null) {
      questionPointStream = StreamSupport.stream(questionPointRepo.findWithinDistance(lat, lng, distance).spliterator(), true);
    } else {
      //If no coordinates are given, we return the first 100 points we find.
      questionPointStream = StreamSupport.stream(questionPointRepo.findAll(new PageRequest(0, 100)).spliterator(), true);
    }

    return questionPointStream.map(q -> q.toResponseType()).collect(Collectors.toList());
  }

}
