package com.quizemon.domain.repo;

import com.arangodb.springframework.annotation.Param;
import com.arangodb.springframework.annotation.Query;
import com.arangodb.springframework.repository.ArangoRepository;
import com.quizemon.domain.dao.QuestionPointDao;

public interface QuestionPointRepo extends ArangoRepository<QuestionPointDao> {

  @Query(
    "FOR q IN WITHIN(questionpoints, @lat, @lng, @distance)" +
      " RETURN q"
  )
  Iterable<QuestionPointDao> findWithinDistance(@Param("lat") Double lat,
                                         @Param("lng") Double lng,
                                         @Param("distance") int distance);
}
